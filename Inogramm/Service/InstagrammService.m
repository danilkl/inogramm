//
//  InstagrammService.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "InstagrammService.h"
#import "Session.h"
#import "Reachability.h"

NSString *InstagrammServiceReachibilityChangedNotification = @"InstagrammServiceReachibilityChangedNotification";

@interface InstagrammService ()

@property (nonatomic, strong) NSURLSession *instagramDataSession;
@property (nonatomic, strong) NSURLSession *imagesDownloadSession;

@property (nonatomic, strong) Session *session;

@property (nonatomic, strong) NSMutableDictionary *imagesDowndloadTasks;

@property (nonatomic, strong) Reachability *reachability;

@end

@implementation InstagrammService

+ (instancetype)sharedService {
    static InstagrammService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[InstagrammService alloc] init];
    });
    return sharedService;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _imagesDowndloadTasks = [[NSMutableDictionary alloc] init];
        
        _session = [[Session alloc] init];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _instagramDataSession = [NSURLSession sessionWithConfiguration:configuration];
        
        _reachability = [Reachability reachabilityForInternetConnection];
        [_reachability startNotifier];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChangedNotification:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
    }
    return self;
}

- (void)loginWithToken:(NSString *)token {
    self.session.token = token;
}

- (BOOL)isLoggedIn {
    return self.session.token.length > 0;
}

- (void)logoutWithCompletion:(InstagrammServiceCompletion)completion {
    self.session.token = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        NSError *errors;
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
        if (completion) {
            completion(YES, nil);
        }
    });
}

- (void)loadMediaWithCompletion:(InstagrammServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.instagram.com/v1/users/self/media/recent/?access_token=%@", self.session.token]];
    
    [[_instagramDataSession dataTaskWithURL:url
                          completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (completion) {
                                      completion(!error, data, error);
                                  }
                              });
                          }] resume];
}

- (NSString *)loadDataWithURL:(NSString *)urlString completion:(InstagrammServiceDataCompletion)completion {
    NSString *UUID = [NSUUID UUID].UUIDString;
    
    NSURLSessionDataTask *task = [_instagramDataSession dataTaskWithURL:[NSURL URLWithString:urlString]
                                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              if (completion) {
                                                                  completion(!error, data, error);
                                                              }
                                                          });
                                                          [self cancelTaskWithUUID:UUID];
                                                      }];
    self.imagesDowndloadTasks[UUID] = task;
    [task resume];
    return UUID;
}

- (void)cancelTaskWithUUID:(NSString *)UUID {
    NSURLSessionDataTask *task = self.imagesDowndloadTasks[UUID];
    [task cancel];
    [self.imagesDowndloadTasks removeObjectForKey:UUID];
}

- (BOOL)online {
    return [self.reachability currentReachabilityStatus] != NotReachable;
}

#pragma mark - Private methods

- (void)reachabilityChangedNotification:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] postNotificationName:InstagrammServiceReachibilityChangedNotification object:nil];
}

@end
