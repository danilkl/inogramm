//
//  InstagrammService.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *InstagrammServiceReachibilityChangedNotification;

typedef void (^InstagrammServiceCompletion) (BOOL success, NSError *error);
typedef void (^InstagrammServiceDataCompletion) (BOOL success, NSData *data, NSError *error);

@interface InstagrammService : NSObject

+ (instancetype)sharedService;

- (BOOL)online;

- (void)loginWithToken:(NSString *)token;

- (BOOL)isLoggedIn;

- (void)logoutWithCompletion:(InstagrammServiceCompletion)completion;

- (void)loadMediaWithCompletion:(InstagrammServiceDataCompletion)completion;

- (NSString *)loadDataWithURL:(NSString *)urlString completion:(InstagrammServiceDataCompletion)completion;

- (void)cancelTaskWithUUID:(NSString *)UUID;

@end
