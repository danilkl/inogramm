


//
//  Mapper.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Mapper.h"

@implementation Mapper

+ (NSArray *)objectsOfClass:(Class<MappingObject>)objectClass
               withJSONData:(NSData *)data
                    keypath:(NSString *)keypath
{
    NSError *error = nil;
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:&error];
    if (error) {
        NSLog(@"%@ %@ %@", NSStringFromClass(self), NSStringFromSelector(_cmd), error);
    }
    NSMutableArray *mutableObjects = [[NSMutableArray alloc] init];
    
    id jsonObject = [JSONDictionary valueForKeyPath:keypath];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        [jsonObject enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [mutableObjects addObject:[self objectOfClass:objectClass withDictionary:obj]];
        }];
    }
    else {
        [mutableObjects addObject:[self objectOfClass:objectClass withDictionary:jsonObject]];
    }
    return [mutableObjects copy];
}

+ (NSArray *)entitiesOfClass:(Class<MappingObject>)etityClass
                withJSONData:(NSData *)data
                     keypath:(NSString *)keypath
                   inContext:(NSManagedObjectContext *)context {
    NSError *error = nil;
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:&error];
    if (error) {
        NSLog(@"%@ %@ %@", NSStringFromClass(self), NSStringFromSelector(_cmd), error);
    }
    NSMutableArray *mutableObjects = [[NSMutableArray alloc] init];
    
    id jsonObject = [JSONDictionary valueForKeyPath:keypath];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        [jsonObject enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [mutableObjects addObject:[self entityOfClass:etityClass withDictionary:obj inContext:context]];
        }];
    }
    else {
        [mutableObjects addObject:[self entityOfClass:etityClass withDictionary:jsonObject inContext:context]];
    }
    return [mutableObjects copy];
}

+ (id<MappingObject>)objectOfClass:(Class)objectClass withDictionary:(NSDictionary *)dictionary {
    NSAssert(![objectClass isSubclassOfClass:[NSManagedObject class]], @"Use entityOfClass:withDictionary:inContext: for creation NSManagedObject subclasses");
    
    NSDictionary *mapping = [objectClass mapping];
    id object = [[objectClass alloc] init];
    
    [self setValuesToObject:object fromDictionary:dictionary withMapping:mapping];
    
    return object;
}

+ (id<MappingObject>)entityOfClass:(Class)entityClass withDictionary:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context {
    NSAssert([entityClass isSubclassOfClass:[NSManagedObject class]], @"Use objectOfClass:withDictionary: for creation non NSManagedObject subclasses");
    
    NSString *entityName = nil;
    
    if ([entityClass respondsToSelector:@selector(entityName)]) {
        entityName = [entityClass entityName];
    }
    
    if (!entityName) {
        entityName = NSStringFromClass(entityClass);
    }
    
    NSString *primaryKey = nil;
    
    if ([entityClass respondsToSelector:@selector(primaryKey)]) {
        primaryKey = [entityClass primaryKey];
    }
    
    NSDictionary *mapping = [entityClass mapping];
    
    __block id entity = nil;
    
    if (primaryKey) {
        __block NSString *primaryKeyMappingKey = nil;
        
        [mapping enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([obj isEqual:primaryKey]) {
                primaryKeyMappingKey = key;
                *stop = YES;
            }
        }];
        
        NSAssert(primaryKeyMappingKey, @"Mapping for primary key not found");
        
        id primaryKeyValue = [dictionary valueForKeyPath:primaryKeyMappingKey];
        NSAssert(primaryKeyValue, @"Primary key value not found");
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", primaryKey, primaryKeyValue];
        fetchRequest.fetchLimit = 1;
        
        [context performBlockAndWait:^{
            NSError *error = nil;
            NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
            if (error) {
                NSLog(@"%@ %@ %@", NSStringFromClass(self), NSStringFromSelector(_cmd), error);
            }
            if (results.count > 0) {
                entity = results.firstObject;
            }
        }];
    }
    
    if (!entity) {
        entity = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    }
    
    [self setValuesToObject:entity fromDictionary:dictionary withMapping:mapping];
    
    return entity;
}

+ (void)setValuesToObject:(id)object fromDictionary:(NSDictionary *)dictionary withMapping:(NSDictionary *)mapping {
    [mapping enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        id value = [dictionary valueForKeyPath:key];
        if (value && ![value isEqual:[NSNull null]]) {
            [object setValue:value forKey:obj];
        }
    }];
}

@end
