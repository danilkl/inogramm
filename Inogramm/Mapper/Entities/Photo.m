//
//  Photo.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Photo.h"

@implementation Photo

+ (NSDictionary *)mapping {
    return @{
             @"images.standard_resolution.url" : @"url",
             @"likes.count" : @"likesCount",
             @"id" : @"photoIdString",
             @"caption.text" : @"detail"
             };
}

+ (NSString *)primaryKey {
    return @"photoIdString";
}

- (void)setPhotoIdString:(NSString *)photoIdString {
    [self willChangeValueForKey:@"photoIdString"];
    [self setPrimitiveValue:photoIdString forKey:@"photoIdString"];
    
    NSArray *idParts = [photoIdString componentsSeparatedByString:@"_"];
    NSAssert(idParts.count == 2, @"Wrong id pattern");
    
    self.photoId = @([idParts[0] integerValue]);
    
    [self didChangeValueForKey:@"photoIdString"];
}


@end
