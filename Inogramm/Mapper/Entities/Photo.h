//
//  Photo.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "MappingObject.h"

@class Comment;

@interface Photo : NSManagedObject <MappingObject>

@end

#import "Photo+CoreDataProperties.h"
