//
//  Pagination.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MappingObject.h"

@interface Pagination : NSObject<MappingObject>

@property (nonatomic, strong) NSString *nextURL;
@property (nonatomic, strong) NSString *nextMaxId;

@property (nonatomic, strong) NSNumber *nextPhotoId;

@end
